import styled from 'styled-components';

export const LoginFormContainerStyled = styled.div`
  width: 100%;
  height: 100%;
  background: rgb(5, 33, 89);
  background: linear-gradient(90deg, rgba(5, 33, 89, 1) 0%, rgba(156, 156, 207, 1) 0%, rgba(32, 27, 205, 1) 100%);
  display: flex;
  align-items: center;
  justify-content: center;
  min-width: 370px;
`;

export const LoginFormBodyStyled = styled.div`
  width: 100%;
  max-width: 1024px;
  display: flex;
  background-color: #ffffff;
  border-radius: 10px;
  box-shadow: 0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24);
  padding: 28px 0;
`;

export const LoginFormHeaderStyled = styled.h2`
  font-weight: 600;
  font-size: 30px;
  color: #4d4dff;
  margin-bottom: 12px;
  text-align: center;
`;

export const LoginFormLeftStyled = styled.div`
  display: flex;
  flex-direction: column;
  row-gap: 8px;
  padding: 8px;
  width: 100%;
`;

export const LoginFormInputStyled = styled.input`
  border: 1px solid #dbdbdb;
  outline: none;
  font-size: 16px;
  letter-spacing: -0.8px;
  width: 100%;
  height: 40px;
  padding: 10px;
  border-radius: 10px;
`;

export const LoginFormButtonStyled = styled.button`
  border: none;
  outline: none;
  cursor: pointer;
  width: 100%;
  height: 46px;
  font-size: 18px;
  letter-spacing: -0.5px;
  background-color: #4d4dff;
  color: white;
  border-radius: 10px;
`;

export const LoginFormRememberContainerStyled = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding: 10px 0;
`;

export const LoginFormLabelStyled = styled.label`
    font-size: 15px;
    letter-spacing: -0.5px;
    cursor: pointer;
`;

export const LoginFormForgotStyled = styled.a`
  font-size: 15px;
  letter-spacing: -0.5px;
  cursor: pointer;
  text-decoration: none;
  font-weight: 500;
  
  &:hover {
    color: #4d4dff;
  }
  
`;

export const LoginFormRightStyled = styled.p`
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  padding: 8px;
  font-weight: 600;
  font-size: 24px;
  flex-basis: 45%;
  text-align: center;
  color: #5e37b8;

  & img {
    object-fit: cover;
    width: 100%;
  }

  @media screen and (max-width: 650px) {
    display: none;
    flex-basis: 0;
  }
`;