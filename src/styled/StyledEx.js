import React, {useState} from 'react';
import styled from 'styled-components';

const Box = styled.div`
  height: 80px;
  background-color: #282c34;
`;

const P = styled.p`
    color: ${props => props.color ? '#ffffff' : '#ffe396'};
    font-weight: 600;
`
export default function StyledEx() {
    const [isColor, setIsColor] = useState(false);
    const handleToggle = () => {
        setIsColor(!isColor);
    };

    return (
        <Box>
            <P color={isColor}>
                스타일드 컴포넌트!
            </P>
            <button onClick={handleToggle}>
                toggle color
            </button>
        </Box>
    )
}