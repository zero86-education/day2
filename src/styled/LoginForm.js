import React, {useState} from 'react';
import {
    LoginFormContainerStyled,
    LoginFormHeaderStyled,
    LoginFormButtonStyled,
    LoginFormInputStyled,
    LoginFormLeftStyled,
    LoginFormRememberContainerStyled,
    LoginFormLabelStyled,
    LoginFormForgotStyled,
    LoginFormRightStyled, LoginFormBodyStyled
} from './LoginForm.styles';
import eliceLogo from './elice_logo.png';

export default function LoginForm() {

    const [userId, setUserId] = useState('');
    const [userPassword, setUserPassword] = useState('');
    const [isRemember, setIsRemember] = useState(false);

    const handleUserIdChange = (e) => {
        setUserId(e.target.value);
    };

    const handleUserPasswordChange = (e) => {
        setUserPassword(e.target.value);
    };

    const handleRememberChange = (e) => {
        setIsRemember(e.target.checked);
    };

    const handleLoginClick = () => {
        if (userId === '') {
            alert('id를 입력해주세요.');
            return false;
        }
        if (userPassword === '') {
            alert('password를 입력해주세요.');
            return false;
        }

        if (isRemember) {
            // id를 기억하는 로직
            console.log(isRemember);
        }

        console.log(userId, userPassword);
    };

    return (
        <LoginFormContainerStyled>
            <LoginFormBodyStyled>
                <LoginFormLeftStyled>
                    <LoginFormHeaderStyled>
                        Login
                    </LoginFormHeaderStyled>
                    <LoginFormInputStyled placeholder="input id" value={userId} onChange={handleUserIdChange}/>
                    <LoginFormInputStyled type="password" placeholder="input password" value={userPassword}
                                          onChange={handleUserPasswordChange}/>
                    <LoginFormRememberContainerStyled>
                        <LoginFormLabelStyled>
                            <input type="checkbox" checked={isRemember} onChange={handleRememberChange}/>
                            <span>Remember</span>
                        </LoginFormLabelStyled>
                        <LoginFormForgotStyled>
                            Forgot Password?
                        </LoginFormForgotStyled>
                    </LoginFormRememberContainerStyled>
                    <LoginFormButtonStyled onClick={handleLoginClick}>Login</LoginFormButtonStyled>
                </LoginFormLeftStyled>
                <LoginFormRightStyled>
                    Welcome Elice :)
                    <img src={eliceLogo} alt="elice-logo"/>
                </LoginFormRightStyled>
            </LoginFormBodyStyled>
        </LoginFormContainerStyled>
    );
}