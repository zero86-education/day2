import React, {useContext, useState} from 'react';
import {ContextEx} from './contextEx';

const MyConsumerComponent = () => {
    // MyComponent 렌더링 여부와 상관없이 Consumer 부분은 다시 렌더링 된다.
    // return(
    //     <div>
    //         <ContextEx.Consumer>
    //             {value => <p>{value}</p>}
    //         </ContextEx.Consumer>
    //     </div>
    // )

    // useContext
    const contextExValue = useContext(ContextEx);
    return (
        <div>
            <p>{contextExValue}</p>
        </div>
    )
};

const MyComponent = React.memo(() => {
    // MyComponent 컴포넌트는 React.memo() 로 감쌌고 속성값이 없기때문에 최초 한번만 렌더링 된다.
    console.log('MyComponent render..');
    return (
        <div>
            <MyConsumerComponent/>
        </div>
    )
});

export default function Main() {
    const [value, setValue] = useState('My Context Value');
    const handleChange = (e) => {
        // 상태값 업데이트
        // Provider 컴포넌트에 해당 상태값을 지정했기 때문에, Provider 컴포넌트 value 도 업데이트가 된다.
      setValue(e.target.value);
    };
    return (
        <ContextEx.Provider value={value}>
            <MyComponent />
            <input value={value} onChange={handleChange}/>
        </ContextEx.Provider>
    );
}