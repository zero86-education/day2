import {createContext} from 'react';

// createContext() 함수를 호출하여 Context 를 생성
export const ContextEx = createContext('default Value');

// 이 Context 구조는 아래와 같습니다.
// createContext => {Provider, Consumer}