import './App.css';
import {Route, Routes} from 'react-router-dom';
import RouterEx from './router/RouterEx';
import RouterEx2 from './router/RouterEx2';
import RouterEx3 from './router/RouterEx3';
import RouterEx4 from './router/RouterEx4';
import LoginForm from './styled/LoginForm';

export default function App() {
    return (
        <Routes>
            <Route path="/" element={<RouterEx/>}/>
            <Route path="/about" element={<h2>회사 소개 페이지</h2>}/>
            <Route path="/events" element={<h2>이벤트 페이지</h2>}/>
            <Route path="/products" element={<h2>상품 페이지</h2>}/>
            <Route path="/customer" element={<h2>고객 센터 페이지</h2>}/>
            <Route path="/dashboard" element={<RouterEx2/>}>
                <Route path="" element={<h2>나는야 대쉬보드</h2>}/>
                <Route path="test" element={<h2>hello dashboard</h2>}/>
            </Route>
            {/* path variable */}
            <Route path="/profile/:id" element={<RouterEx3/>} />
            {/* query string */}
            <Route path="/play?id=1&name=query" element={<RouterEx4/>} />
            <Route path="/login" element={<LoginForm/>}/>
        </Routes>
);
}
