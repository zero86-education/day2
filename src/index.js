import React from 'react';
import ReactDOM from 'react-dom/client';
import {BrowserRouter} from 'react-router-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
// import CssEx from './css/CssEx';
// import CssModuleEx from './css-module/CssModuleEx';
// import ScssEx from './scss/ScssEx';
// import StyledEx from './styled/StyledEx';
// import UseStateEx4 from './hooks/UseStateEx4';
// import UseRefEx from './hooks/UseRefEx';
// import UseRefEx2 from './hooks/UseRefEx2';
// import UseRefEx3 from './hooks/UseRefEx3';
// import UseMemoEx from './hooks/UseMemoEx';
// import UseCallbackEx from './hooks/UseCallbackEx';
// import Main from './context/Main';
// import FragmentEx from './fragment/FragmentEx';
// import RouterEx from './router/RouterEx';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <BrowserRouter>
    <App />
  </BrowserRouter>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();


