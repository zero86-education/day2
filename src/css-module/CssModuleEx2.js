import React from 'react'
import cn from 'classnames';

// {이름}.module.css
import styles from './CssModuleEx.module.css'

export default function CssModuleEx() {
    return (
        <button className={cn(styles.button)}>
            Hello
        </button>
    )
}