import React from 'react'

// {이름}.module.css
import styles from './CssModuleEx.module.css'

export default function CssModuleEx() {
    return (
        <button className={styles.button}>
            Hello
        </button>
    )
}