import React, {useCallback, useState} from 'react';


const Child = React.memo(() => {

    // 매번 새로운 func 레퍼런스를 받기 때문에, React.memo() 를 사용했어도 렌더링이 발생
    console.log('Child render...');

    return (
        <div>
            Child Component
        </div>
    );
});

const Child2 = React.memo(() => {

    console.log('Child2 render...');

    return (
        <div>
            Child2 Component
        </div>
    );
});

export default function UseCallbackEx() {

    const [text, setText] = useState('');


    // 첫번째 인자로 함수를 받는다. 생성된 함수 레퍼런스를 기억
    // 두번째 인자로 의존성 배열을 받는다. 의존성 배열이 변경되지 않으면 이전에 반환된 함수 레퍼런스 재사용
    // 배열이 그냥 빈 배열이면 처음에 생성한 함수 레퍼런스 재사용
    const funcCallback = useCallback(() => {
        alert('memoFunc!');
    }, []);

    const func = () => {
        alert('func!');
    };

    const handleChange = (e) => {
        setText(e.target.value);
    };

    return (
        <div>
            <Child func={func}/>
            <Child2 func={funcCallback}/>
            <input value={text} onChange={handleChange}/>
        </div>
    );

}