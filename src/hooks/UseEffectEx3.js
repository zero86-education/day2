import React, {useState, useEffect} from 'react';

export default function UseEffectEx3() {
    const [input, setInput] = useState('');

    console.log(1);

    useEffect(() => {
        console.log('mount..');
        return () => console.log('unmount..', input);
    }, [input]);

    console.log(2);

    return (
        <div>
            <p>UseEffectEx3 Component</p>
            <input value={input} onChange={e => setInput(e.target.value)}/>
        </div>
    )
}