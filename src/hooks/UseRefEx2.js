import React, {useEffect, useRef} from 'react';

// useRef() DOM 요소 접근
export default function UseRefEx2() {
    const ref = useRef();

    useEffect(() => {
        console.log(ref);
        ref.current.focus();
    }, []);

    const handleClick = () => {
        alert(`환영합니다! ${ref.current.value}`);
        ref.current.value = '';
    };

    return (
        <div>
            <input ref={ref} placeholder="username"/>
            <button onClick={handleClick}>alert!</button>
        </div>
    );
}