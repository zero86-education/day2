// react 패키지 임포트
import React, {useState} from 'react';

const heavyWork = () => {
  console.log('나는 무거운 작업');
  return ['홍길동']
};

export default function UseStateEx3() {
    // 컴포넌트 상태값 정의
    // const [names, setNames] = useState(heavyWork()); // 이런식으로 사용하면, 렌더링이 될때마다 함수가 호출되어 성능에 좋지 않음
    const [names, setNames] = useState(() => heavyWork()); // useState() 에도 콜백을 사용가능한데, 콜백형태로 사용하면 한번만 실행이 된다.
    const [input, setInput] = useState('');

    const handleChange = (e) => {
        // 입력을 받을 때 마다 업데이트
        setInput(e.target.value);
    };

    const handleClick = () => {
        if(input !== '') {
            // input 값이 비어있지 않다면, 업데이트
            setNames([
                ...names, input
            ]);
            
            // 상태 업데이트 함수에 콜백함수 형태로 작성이 가능
            // 해당 함수의 매개변수는 현재 상태값을 가짐
            // setNames(prevState => {
            //     return [...prevState, input]
            // })
        }
    };

    console.log('update UseStateEx3 component..');

    return (
        <div>
            <input type="text" value={input} onChange={handleChange}/>
            <button onClick={handleClick}>Add</button>
            {names.map((name, index) => {
                return (
                    <p key={index}>
                        {name}
                    </p>
                );
            })}
        </div>
    );
}