// react 패키지 임포트
import React, {useState} from 'react';

export default function UseStateEx() {
    // 컴포넌트 상태값 정의
    const [time, setTime] = useState(1);

    const handleClick = () => {
        let newTime;
        // 12시 이후에 13시로 표기가 되는게 아닌, 1시로 표기
        if(time >= 12) {
            newTime = 1;
        } else {
            newTime = time + 1;
        }
        setTime(newTime); // 현재 시간에서 1 증가 시킴
        // 다시 렌더링이 된다.
    };

    console.log('update UseStateEx component..');

    return (
        <div>
            <span>현재 시각: {time}시</span>
            <button onClick={handleClick}>시간 최신화!</button>
        </div>
    )
}