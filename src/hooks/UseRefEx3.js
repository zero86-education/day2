import React, {useCallback, useState} from 'react';

// ref 속성 함수 지정

const INIT_TEXT = 'hello world!';
const INIT_TEXT2 = 'hello world2!';
export default function UseRefEx3() {

    const [text, setText] = useState('');
    const [text2, setText2] = useState('');
    const [isShow, setIsShow] = useState(true);

    const handleChange = (e) => {
        setText(e.target.value);
    };

    const handleClick = () => {
      setIsShow(!isShow);
    };

    // 컴포넌트가 렌더링 될 때마다, INIT_TEXT 가 입력되기 때문에 원하는 대로 동작을 하지 않는다.
    // 이러한 문제를 해결하려면, 함수를 고정 시키면 된다.

    const refInit = useCallback((ref) => {
        if(ref) setText2(INIT_TEXT2);
    }, []);

    const handleChange2 = (e) => {
        setText2(e.target.value);
    };

    return (
        <div>
            {
                isShow && (
                    <div>
                    <input
                        type="text"
                        ref={ref => ref && setText(INIT_TEXT)}
                        value={text}
                        onChange={handleChange}
                    />
                        <input
                            type="text"
                            ref={refInit}
                            value={text2}
                            onChange={handleChange2}
                        />
                    </div>
                )
            }
            <button onClick={handleClick}>show toggle</button>
        </div>
    );
}