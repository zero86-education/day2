// react 패키지 임포트
import React, {useState} from 'react';

export default function UseStateEx4() {
    // 컴포넌트 상태값 정의
    const [count, setCount] = useState(1);

    const handleClick = () => {
        // 상태값 변경 함수를 연속해서 호출
        setCount(count + 1);
        setCount(count + 1);
    };

    const handleClick2 = () => {
        // 상태값 변경 함수를 연속해서 호출
        // 콜백 함수 형태로 작성
        setCount((prevState) => prevState + 1);
        setCount((prevState) => prevState + 1);
        // 이러면 아래 콘솔이 2번 연속 찍힐거 같은데, 1번만 찍힌다.
        // 상태값 업데이트를 일괄로 처리하기 때문이다.
    };

    console.log('update UseStateEx4 component..');

    return (
        <div>
            <p>{count}</p>
            <button onClick={handleClick}>Increment</button>
            <button onClick={handleClick2}>Increment2</button>
        </div>
    );
}