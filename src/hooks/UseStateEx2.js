// react 패키지 임포트
import React, {useState} from 'react';

export default function UseStateEx2() {
    // 컴포넌트 상태값 정의
    const [names, setNames] = useState(['홍길동']);
    const [input, setInput] = useState('');

    const handleChange = (e) => {
        // 입력을 받을 때 마다 업데이트
        setInput(e.target.value);
    };

    const handleClick = () => {
        if(input !== '') {
            // input 값이 비어있지 않다면, 업데이트
            setNames([
                ...names, input
            ]);
            
            // 상태 업데이트 함수에 콜백함수 형태로 작성이 가능
            // 해당 함수의 매개변수는 현재 상태값을 가짐
            // setNames(prevState => {
            //     return [...prevState, input]
            // })
        }
    };

    console.log('update UseStateEx2 component..');

    return (
        <div>
            <input type="text" value={input} onChange={handleChange}/>
            <button onClick={handleClick}>Add</button>
            {names.map((name, index) => {
                return (
                    <p key={index}>
                        {name}
                    </p>
                );
            })}
        </div>
    );
}