import React, {useEffect, useState} from 'react';

export default function UseEffectEx() {

    const [num, setNum] = useState(0);

    // 렌더링 될때 마다 실행
    useEffect(() => {
        console.log('useEffect..1');
    });

    // 컴포넌트가 Mount 됬을 대 실행(첫 렌더링)
    useEffect(() => {
        console.log('useEffect..2');

        // clean up(정리 작업)
        return () => {
            // 정리하는 작업 코드 작성
        }
    }, []);

    // 컴포넌트가 Mount 됫을 때 + num 값이 바뀔 때
    useEffect(() => {
        console.log('useEffect..3');
    }, [num]);


    return (
        <h2>
            나는야 UseEffectEx
            <p>{num}</p>
            <button onClick={() => setNum(num + 1)}>Increment</button>
        </h2>
    )
}