import React, {useState, useMemo} from 'react';


// useMemo() 훅 예제
export default function UseMemoEx() {
    const [number, setNumber] = useState(1);
    const [text, setText] = useState('');

    // 첫번째 인자로 함수를 받는다. useMemo() 훅은 이 함수가 반환하는 값을 기억
    // 두번째 인자로 의존성 배열을 받는다. 의존성 배열이 변경되지 않으면 이전에 반환된 값을 재사용
    // 배열이 그냥 빈 배열이면 한번 연산한 값을 계속 재사용
    const memoNumber = useMemo(() => {
        console.log('useMemo..');
        return number * 100;
    }, [number]);


    // text 를 업데이트 하더라도, useMemo 함수는 다시 실행되지 않는다. 그저 이전 값을 기억해서 재사용 한다.
    const handleChange = (e) => {
        setText(e.target.value);
    };

    return (
        <div>
            <div style={{boarder: '1px dotted skyblue'}}>
                <h2>useMemo</h2>
                <p>
                    useMemo 훅은 계산량이 많은 함수의 반환값을 재활용하는 용도로 사용
                </p>
                <p>
                    {`memoNumber : ${memoNumber}`}
                </p>
            </div>
            <input value={text} onChange={handleChange}/>
        </div>
    );
}
