import React, {useEffect} from 'react';

export default function UseEffectEx2() {
    console.log(1);

    useEffect(() => {
        console.log('mount..');
    }, []);

    console.log(2);

    return <div>UseEffectEx2 Component</div>
}