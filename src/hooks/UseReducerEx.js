import React, {useReducer} from 'react';


// 초기 상태값
const INITIAL_STATE = {
    name: 'test',
    age: 10,
};


// 리듀서 함수
// 컴포넌트의 상태가 어떤 액션에 따라 변화하는지 정의
const reducer = (state, action) => {
    switch (action.type) {
        case 'setName' :
            return {
                ...state,
                name: action.name
            };
        case 'setAge' :
            return {
                ...state,
                age: action.age
            };
        default:
            return state;
    }
};

export default function UseReducerEx() {

    // useReducer() 훅은 매개변수로 리듀서와 초기 상태값을 받는다.
    // 상태값과 dispatch 함수를 반환한다.
    // dispatch 함수는 액션을 일으켜 리듀서 함수가 실행되도록 합니다.
    // 액션은 업데이트 정보를 가지고 있는 것
    const [state, dispatch] = useReducer(reducer, INITIAL_STATE);

    const handleNameChange = (e) => {
        dispatch({
            type: 'setName',
            name: e.target.valueOf()
        });
    };

    const handleAgeChange = (e) => {
        dispatch({
            type: 'setAge',
            age: e.target.valueOf()
        })
    };

    return (
        <div>
            <p>name: {state.name}</p>
            <p>age: {state.age}</p>
            <div>
                <input value={state.name} onChange={handleNameChange}/>
            </div>
            <div>
                <input value={state.age} onChange={handleAgeChange}/>
            </div>
        </div>
    )
}