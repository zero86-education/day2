import React, {useCallback, useEffect, useState} from 'react';

// custom hook
const useToggle = (initialState = false) => {
    const [state, setState] = useState(initialState);
    const toggle = useCallback(() => setState(state => !state), []);

    useEffect(() => {
        console.log('useToggle..');
    }, []);

    return [state, toggle]
}
export default function UseCustomHookEx() {
    const [isTextChanged, setIsTextChanged] = useToggle();

    useEffect(() => {
        console.log('UseCustomHookEx..');
    }, []);

    return (
        <button onClick={setIsTextChanged}>{isTextChanged ? 'Toggled' : 'Click to Toggle'}</button>
    );
}