import React, {useRef, useState} from 'react';

// useRef() 값 저장 및 렌더링
export default function UseRefEx() {
    const [count, setCount] = useState(0);
    const countRef = useRef(0);

    const ref = useRef('hi'); // useRef() 훅을 호출하면, ref 객체를 반환
    // { current: value } // ref object 구조
    // value 는 초기값 => hi
    console.log(ref.current); // hi

    // ref.current 값은 수정이 가능
    ref.current = 'hello';
    console.log(ref.current); // hello

    const handleClick = () => {
        setCount(count + 1);
    };

    const handleClick2 = () => {
        countRef.current += 1; // 해당 값을 업데이트 하더라도, 렌더링이 발생하지 않는다.
        console.log(countRef);
    }

    console.log('UseRefEx Render..');
    
    // 컴포넌트가 상태값이 변경되어 렌더링이 다시 발생이 된다는 의미는 함수가 재실행 된다는 의미

    return (
        <div>
            <p>count: {count}</p>
            <button onClick={handleClick}>Increment</button>
            <button onClick={handleClick2}>Increment</button>
        </div>
    );
}