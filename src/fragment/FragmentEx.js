import React from 'react';

const Component = () => {
    // 반드시 단 하나의 최상위 태그로 묶여 있어야 함.
    return (
        <div>
            <h1>hello! Component</h1>
            <p>엘리스 파이팅!</p>
        </div>
    )
};

// const Component2 = () => {
//     // 이런식으로 사용하면 에러가 발생한다.
//     return (
//         <h1>hello! Component</h1>
//         <p>엘리스 파이팅!</p>
//     )
// };

export default function FragmentEx() {
    return (
        <React.Fragment>
            <h1>hello! Component</h1>
            <p>엘리스 파이팅!</p>
        </React.Fragment>
    )
}