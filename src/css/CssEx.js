import React from 'react'

// 일반적인 css 파일
// css 파일도 ESM 문법을 통해 가져올 수 있음
import './CssEx.css';

export default function CssEx() {
    return (
        <button className='button'>
            Hello
        </button>
    )
}