import React from 'react';

// Link 컴포넌트 사용해보기
import {Link, useNavigate} from 'react-router-dom';

export default function RouterEx() {
    const navigate = useNavigate();
    const handleClick = (e) => {
        e.preventDefault();
        navigate('/customer'); // 프로그래밍 방식으로 제어
    };

    return (
        <div>
            <header style={{
                border: '1px dotted skyblue'
            }}>
                <nav style={{
                    padding: 10,
                    display: 'flex',
                    alignItems: 'center',
                    justifyContent: 'center'
                }}>
                    <Link style={{
                        cursor: 'pointer',
                        padding: 8
                    }} to="/about">회사 소개</Link>
                    <Link style={{
                        cursor: 'pointer',
                        padding: 8
                    }} to="/events">이벤트</Link>
                    <Link style={{
                        cursor: 'pointer',
                        padding: 8
                    }} to="/products">제품</Link>
                    <a style={{
                        cursor: 'pointer',
                        padding: 8
                    }} onClick={handleClick}>고객 지원</a>
                </nav>
            </header>
        </div>
    );
}