import React from 'react';
import {useSearchParams} from 'react-router-dom';

// query string
export default function RouterEx4() {
    const [searchParams, setSearchParams] = useSearchParams();
    const id = searchParams.get('id');
    const name = searchParams.get('name');
    return (
        <div>
            <p>
                play id: {id} / name: {name}
            </p>
        </div>
    );
}