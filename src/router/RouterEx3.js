import React from 'react'
import {useParams} from 'react-router-dom';

// path variable
export default function RouterEx3() {
    const { id } = useParams();
    return (
        <div>
            <p>
                {id}의 profile page
            </p>
        </div>
    )
}