import React from 'react';
// outlet
import {Outlet} from 'react-router-dom';

export default function RouterEx2() {
    return (
        <div>
            <h1>Dashboard</h1>
            <Outlet/>
        </div>
    )
}